#include "division_test.h"
#include "arithmetique.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <limits>

// Enregistrer la classe de test dans le registre de la suite
CPPUNIT_TEST_SUITE_REGISTRATION(divisionTest);

void divisionTest::setUp() {}

void divisionTest::tearDown() {}

/* TODO: Ajouter les définitions des méthodes de test de la classe
 * divisionTest
 */

void divisionTest::division_normale()
{
  dividende = 8;
  diviseur = 2;
  CPPUNIT_ASSERT_EQUAL((long)4, arithmetique::division(dividende, diviseur));
  diviseur = -2;
  CPPUNIT_ASSERT_EQUAL((long)-4, arithmetique::division(dividende, diviseur));
}

void divisionTest::division_par_zero()
{
  dividende = 123;
  diviseur = 0;
  CPPUNIT_ASSERT_THROW(arithmetique::division(dividende, diviseur), std::exception);
}

void divisionTest::division_min()
{
  dividende = std::numeric_limits<int>::lowest();
  diviseur = -1;
  CPPUNIT_ASSERT_EQUAL(-(long)dividende, (long)arithmetique::division(dividende, diviseur));
}

void divisionTest::division_max()
{
  dividende = std::numeric_limits<int>::max();
  diviseur = 1;
  CPPUNIT_ASSERT_EQUAL((long)dividende, arithmetique::division(dividende, diviseur));
}


int main()
{
  // Obtenir le registre de tests
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
  // Obtenir la suite principale depuis le registre
  CppUnit::Test *suite = registry.makeTest();
  // Ajouter la suite à l'exécutant des tests
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(suite);
  // Définir l'utilitaire de sortie au format "erreurs de compilation"
  runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
                      std::cerr));
  // Lancer l'exécution des tests
  bool wasSucessful = runner.run();
  // Retourner le code d'erreur 1 si l'un des tests a échoué
  return wasSucessful ? 0 : 1;
}
