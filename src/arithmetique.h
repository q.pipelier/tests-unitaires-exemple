#ifndef ARITHMETIQUE_H
#define ARITHMETIQUE_H

namespace arithmetique {
  long int addition(int, int);
  int soustraction(int, int);
  int multiplication(int, int);
  long division(int, int);
}

#endif // ARITHMETIQUE_H
